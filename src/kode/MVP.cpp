/**
 *  The minimal viable product of Group 4's product
 * @file MVP.cpp
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/*
#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
using namespace std;


class Tournament
{
private:

public:
void theTournament();
};

class Player : public Tournament
{
private:
    string name;
    int rating;
    int playerID;
    string club;
    int wins,
        draws;
    float score;

public:
                        // sets all data as empty or 0
Player() {name = club = ""; rating = playerID = wins = draws = score = 0;};

void newPlayer(const string nme);
void writePlayer()  const;
string returnName();
float returnScore();
void editSelectedPlayer();
void addWin();
void addDraw();
void calculateScore();
};

class Match : public Tournament {
private:
    vector <int *> tableNr;

};

// Global values
const int STRLEN = 100;
int gRounds = 0;
int gTables = 0;

// Vectors
vector<Player *> gPlayers;
vector <Player *> player1;
vector <Player *> player2;


// global functions
void addPlayer();
void writeMenu();
void editPlayer();
void viewPlayers();
Player *findPlayer(const string name);
void newMatch();
void printScore();

int main() {

char command;
    //readFromFile();
    //readFromFileTournament();

    //amntTables();

    writeMenu();
    command = lesChar("\nCommand");

    while (command != 'Q')
    {
        switch (command)
        {
        case 'A':       addPlayer();        break;
        case 'W':       viewPlayers();      break;
        case 'E':       editPlayer();       break;
        case 'M':       newMatch();         break;
        case 'V':       printScore();       break;
        default:        writeMenu();        break;
        }
        command = lesChar("\nCommand");
    }

      //writeToFileTournament();
      //writeToFile();

    return 0;
}


void Player::newPlayer(const string nme) {
    name = nme;

    rating = lesInt("\n\tPlayers rating: ", 1, 3000);

    playerID = lesInt("\n\tPlayers ID: ", 1, 10000);

    cout << "\n\tPlayers club: ";   getline(cin, club);
}
*/
/**
 * edits selected player from list
 * 
 */
 /*
void Player::editSelectedPlayer() {
    cout << "\n\tPlayers Name: ";   getline(cin, name);

    rating = lesInt("\n\tPlayers rating: ", 1, 3000);

    playerID = lesInt("\n\tPlayers ID: ", 1, 10000);

    cout << "\n\tPlayers club: ";   getline(cin, club);

}

*/
/*
 * Writes out all info on player
 *
 */
/*
void Player::writePlayer() const
{

    cout << "\n"
         << "Player name:       " << name << '\n'
         << "Player rating:     " << rating << '\n'
         << "PlayerID:          " << playerID << '\n'
         << "Players club:      " << club << '\n'
         << "Amount of draws:   " << draws << '\n'
         << "Amount of wins:    " << wins << "\n\n";
}
*/
/*
 * Finds player based on name and returns pointer
 *
 * @param name
 * @return Player*
 *//*
Player *findPlayer(const string name)
{
    for (size_t i = 0; i < gPlayers.size(); i++)
    {
        if (gPlayers[i]->returnName() == name)
            return gPlayers[i];
    }
    return nullptr;
}

string Player::returnName(){
    return name;
}

float Player::returnScore(){
    return score;
}

void Player::addWin() {
    wins++;
}

void Player::addDraw() {
    draws++;
}

void Player::calculateScore()
{
    score = draws*0.5 + wins*1;
}


void Tournament::theTournament() {
     int matches;
 matches = lesInt("\n\tHow many matches wil be played: ", 2, (gPlayers.size() / 2));
}



 //=============================================================================
 //
 // NON CLASS FUNCTIONS
 //
 // ============================================================================




*/
/**
 * Add a player
 *
 * @see - Player::newPlayer()
 */
 /*
void addPlayer()
{
    Player *newPlayer;
    string name;

    newPlayer = new Player;
    cout << "\n\tnew players name: "; getline(cin, name);
    newPlayer->newPlayer(name);

    gPlayers.push_back(newPlayer);
    cout << "\n\nPlayer added!\n\n";
}
*/
/**
 * Writes out all players and their stats
 * 
 * @see writePlayer() const
 *//*
void viewPlayers() {

    for(size_t i = 0; i < gPlayers.size(); i++){
        cout << "\n\tPlayer " << i+1; 
        gPlayers[i]->writePlayer();
    }
}
*/
/**
 * find player to edit
 * 
 * @see viewPlayers()
 * @see editSelectedPlayer()
 * @see returnName()
 *//*
void editPlayer()   {

    int choise,
        playerNr;
    string name;

    choise = lesInt("\n\t1. Enter info  2. List", 1, 2);

                        // searches after player by name
    if(choise == 1) {
        cout << "\n\tPlayers name: ";   getline(cin, name);
        for(size_t i = 0; i < gPlayers.size(); i++) {
            if(gPlayers[i]->returnName() == name) {
                gPlayers[i]->editSelectedPlayer();
            }
        }
                        // pickes player out from list
    } else if(choise == 2) {
        viewPlayers();
        playerNr = lesInt("\n\tPlayer: ", 1, gPlayers.size());
        for(size_t i = 0; i < gPlayers.size(); i++) {
            if(gPlayers[playerNr-1]->returnName() == gPlayers[i]->returnName())
            {
                gPlayers[i]->editSelectedPlayer();
            }
        }
    }
}
*/
/**
 * Sets up a new match
 * 
 *//*
void newMatch() {
    string name;
    int WorD,
        who;

    viewPlayers();

    cout << "\n\tPlayer1 name: ";   getline(cin, name);
    for(size_t i = 0; i < gPlayers.size(); i++) {
        if(gPlayers[i]->returnName() == name) {
            player1.push_back(gPlayers[i]);
        }  
    }

    cout << "\n\tPlayer2 name: ";   getline(cin, name);
    for(size_t i = 0; i < gPlayers.size(); i++) {
        if(gPlayers[i]->returnName() == name) {
            player2.push_back(gPlayers[i]);
        }  
    }

    WorD = lesInt("\n\tWas there a 1. win or 2. draw", 1, 2);
    if(WorD == 2){
        player1[0]->addDraw();
        player2[0]->addDraw();
        cout << "\n\tMatch was a draw! both players rewarded 0.5 points\n\n";
    }else if(WorD == 1) {
        cout << "\n\tWho won 1. " << player1[0]->returnName() << " or 2. " << player2[0]->returnName();   who = lesInt(": ",1,2);
        if(who == 1) {
            player1[0]->addWin();
        } else {
            player2[0]->addWin();
        }
    }

}

void printScore(){
        cout<<"\nResults:\n"
            <<"\tName: \tScore:\n";
    for(size_t i = 0; i < gPlayers.size(); i++){
        gPlayers[i]->calculateScore();
        cout<<"\t" <<gPlayers[i]->returnName() <<"\t" <<gPlayers[i]->returnScore() <<"\n";
    }
}
*/
/**
 * writes the programs menu options
 *//*
void writeMenu()
{
    cout << "\nMenu: \n"
         << "\tA - Add player\n"
         << "\tW - view players\n"
         << "\tE - Edit player\n"
         << "\tM - New match\n"
         << "\tV - View results\n"
         << "\tQ - Quit\n\n";
}


*/