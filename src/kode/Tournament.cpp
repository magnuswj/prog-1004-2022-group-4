//
// Created by sigru on 26.04.2022.
//

#include "Tournament.h"

#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"
#include "functions.h"

using namespace std;

extern vector<Tournament *> gTournaments;
extern vector<Player*> playerList;
extern vector<Player*> sortedRankings;

void Tournament::enterResults()
{
    if (playerList.size()==0)
    {
        cout << endl << "No player's exist.";
        return;
    }
    cout << endl << "Enter results for round " << currentRound;
    for (int i = 0; i < nrOfTables; ++i)
    {
        playerList[searchByID(white[i],playerList)]->playedAgainst.push_back( playerList[searchByID(black[i],playerList)]->playerID);
        playerList[searchByID(black[i],playerList)]->playedAgainst.push_back(playerList[searchByID(white[i],playerList)]->playerID);
        playerList[searchByID(white[i],playerList)]->white++;
        int answer;
        cout << endl << "Enter results for table " << i+1 << " with "
        << playerList[searchByID(white[i],playerList)]->lastName
        << " as white and "
        << playerList[searchByID(black[i],playerList)]->lastName
        << " as black."
        << endl << "1: white victory"
        << endl << "2: black victory"
        << endl << "3: draw";
        answer = lesInt("\nResult: ",1,3);
        switch (answer)
        {
            case 1:
                playerList[searchByID(white[i],playerList)]->results.push_back(2);
                playerList[searchByID(black[i],playerList)]->results.push_back(0);
                break;
            case 2:
                playerList[searchByID(white[i],playerList)]->results.push_back(0);
                playerList[searchByID(black[i],playerList)]->results.push_back(2);
                break;
            case 3:
                playerList[searchByID(white[i],playerList)]->results.push_back(1);
                playerList[searchByID(black[i],playerList)]->results.push_back(1);
                break;
            default:
                cout << "Error!";
                break;
        }
    }
    sortRanking();

    if(currentRound == nrOfRounds)
    {
        finished=true;
        endTournament();
    }
    else
    {
        printTables();
    }
}

void Tournament::printTables()
{
    if (playerList.size()==0)
    {
        cout << endl << "No player's exist.";
        return;
    }

    if(canStartRound() == false)
    {
        cout << endl << "There number of tables does not correspond to the amount of players,"
             << " please add or remove players so that there is exactly two players per table." << endl;
        return;
    }
    white.clear();
    black.clear();

    currentRound++;


    if (currentRound == 1)
    {
        vector<Player*> playersPlaying = playerList;
        for (int i = 0; i < nrOfTables; ++i)
        {
            white.push_back(playersPlaying[playersPlaying.size()-1]->playerID);
            playersPlaying.pop_back();
            black.push_back(playersPlaying[playersPlaying.size()-1]->playerID);
            playersPlaying.pop_back();
        }
    }
    else
    {
        createMatchups();
    }
    cout << endl << "Table \t\t White Player \t\t Black Player";
    for (int i = 0; i < nrOfTables; ++i)
    {
        cout << endl << i+1 << "\t\t"
        << playerList[searchByID(white[i],playerList)]->lastName
        << "\t\t" << playerList[searchByID(black[i],playerList)]->lastName;
    }
}


void Tournament::createMatchups()
{
    Player* currentPlayer;
    Player* opponent;
    vector<Player*> playersPlaying = sortedRankings;
    sortRanking();
    for (int i = 0; i < nrOfTables; i++)
    {
        currentPlayer = playersPlaying[0];
        for (int j = 1; j < playersPlaying.size(); j++) //finds the players opponent
        {

                if (!currentPlayer->hasPlayedAgainst(playersPlaying[j]->playerID))
                {
                    opponent=playersPlaying[j];
                    for (int k = j; k < playersPlaying.size(); ++k)
                    {
                        playersPlaying[k]=playersPlaying[k+1];
                    }
                    playersPlaying.pop_back();
                    for (int k = 0; k < playersPlaying.size(); ++k)
                    {
                        playersPlaying[k]=playersPlaying[k+1];
                    }
                    playersPlaying.pop_back();
                    j=playersPlaying.size(); //removes the opponents from the list and exists the loop
                }

        }
        if (currentPlayer->white > opponent->white || currentPlayer->white == opponent->white)
        {
            white.push_back(opponent->playerID);
            black.push_back(currentPlayer->playerID);
        }
        else if (currentPlayer->white < opponent->white)
        {
            white.push_back(currentPlayer->playerID);
            black.push_back(opponent->playerID);
        }
    }
}

bool Tournament::canStartRound()
{
    if (playerList.size() == nrOfTables*2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Tournament::endTournament()
{
    cout << endl << endl << "The tournament is finished! Here are the final rankings:";
    printRanking();
}

/**
 *   Writes all data about the tournament rounds to file
 */
void Tournament::writeToFileTournaments(ofstream &out)
{

    out << nrOfTables << '\n';
    out << currentRound << '\n';
    out << nrOfRounds << '\n';
    for(int i = 0; i < white.size(); i++)
        out << white[i];
    for(int i = 0; i < black.size(); i++)
        out << black[i];
    out << finished;
}

/**
 *  Reads all data about the tournament rounds from file
 */
void Tournament::readFromFileTournaments(ifstream &in)
{
    int ID;

    in >> nrOfTables;   in.ignore();
    in >> currentRound; in.ignore();
    in >> nrOfRounds;   in.ignore();
    for(int i = 0; i < nrOfTables; i++)
        in >> ID;
        white.push_back(ID);
    in.ignore();
    for(int i = 0; i < nrOfTables; i++)
        in >> ID;
        black.push_back(ID);
}
