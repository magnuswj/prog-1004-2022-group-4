//
// Created by sigru on 26.04.2022.
//

#ifndef CHECKMATE_TOURNAMENT_H
#define CHECKMATE_TOURNAMENT_H

#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"

using namespace std;

extern vector<Player*> playerList;
extern vector<Player*> sortedRankings;

class Tournament
 {

    public :
        int nrOfTables; //amount of tables in the tournament
        int currentRound; //which round we are currently in, is updated in Tournament::printTables
        int nrOfRounds; //amount of rounds the tournament has total
        vector<int>white; //id of the players with white pieces in the current round, so white[0] is the id of the player with white pieces on table 1
        vector<int>black; //id of the players with black pieces in the current round, so black[0] is the id of the player with black pieces on table 1
        bool finished;
        //white and black are reset each round, but still needs to be saved

        virtual void enterResults(); //Enters the results after a round, goes through each table and asks the result before allowing the next round to start
        virtual void endTournament(); //Goes if currentround is the final round, prints the final results
        virtual void printTables(); //Calls createMatchups and prints the tables
        virtual void createMatchups();//Figures out who should play against each other
        bool canStartRound();//Controls that the amount of tables and players correspond
        void writeToFileTournaments(ofstream &out); //-
        void readFromFileTournaments(ifstream &in); //-

};


#endif //CHECKMATE_TOURNAMENT_H
