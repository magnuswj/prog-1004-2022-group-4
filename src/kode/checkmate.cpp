#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"
#include "Tournament.h"
#include "functions.h"

using namespace std;

vector<Player*> playerList;
vector<Player*> sortedRankings;


vector<Tournament *> gTournaments;

string menuToPrint;
string menuStart = "\nS: Start a new tournament\nQ: Quit the program";
string menuSetUp = "\nV: View player list\nE: Edit a player\nA: Add player\nD: Delete player\nF: Start the first round\nS: Start a new tournament\nQ: Quit the program";
string menuDuring = "\nV: View player list\nR: View rankings\nE: Edit a player\nN: Enter results and advance to the next round\nS: Start a new tournament\nQ: Quit the program";
string menuEnd = "\nS: Start a new tournament\nR: View rankings\nV: View player list\nQ: Quit the program";


int main()
{
    char command;

    readFromFile();
    readFromFileTournament();
    cout << endl << "Welcome to checkmate, a program for organizing chess tournaments. "
    << "Please use a single character as shown in the text when interacting with the program. "
    << "If you encounter any issues, please take contact with either the leader of the team, "
    << "Loke Svelland at lokens@stud.ntnu.no, "
    << "or the lead programmer, Sigrun Hogstedt at sigruah@stud.ntnu.no." << endl;

    do
    {
        if (gTournaments.empty())
        {
            menuToPrint=menuStart;
        }
        else if (gTournaments[0]->currentRound==0)
        {
            menuToPrint=menuSetUp;
        }
        else if (gTournaments[0]->finished)
        {
            menuToPrint=menuEnd;
        }
        else
        {
            menuToPrint=menuDuring;
        }

        cout << endl << menuToPrint;
        command = lesChar("\nCommand: ");
       // cout << command;
        if (menuToPrint==menuStart)
        {
            switch (command)
            {
                case 'S':
                    startNewTournament();
                    break;
                case 'Q':
                    cout << endl << "Closing the program";
                    break;
                default:
                    cout << endl << "Please enter a valid command.";
                    break;
            }
        }
        else if (menuToPrint==menuSetUp)
        {
            switch (command)
            {
                case 'S':
                    startNewTournament();
                    break;
                case 'V':
                    viewPlayers();
                    break;
                case 'E':
                    editPlayer();
                    break;
                case 'A':
                    if (playerList.size() == gTournaments[0]->nrOfTables*2)
                    {
                        cout << endl << "There are already enough players, either reset tournament and add more tables"
                        << " or delete an existing player to add more.";
                    }
                    else
                    {
                        int id;
                        id = lesInt("\nWhat is the new player's ID?\n",0,10000);
                        newPlayer(id);
                    }
                    break;
                case 'D':
                    deletePlayer();
                    break;
                case 'Q':
                    cout << endl << "Closing the program";
                    break;
                case 'F':
                    gTournaments[0]->printTables();
                    break;
                default:
                    cout << endl << "Please enter a valid command.";
                    break;
            }
        }
        else if (menuToPrint==menuDuring)
        {
            switch (command)
            {
                case 'S':
                    startNewTournament();
                    break;
                case 'V':
                    viewPlayers();
                    break;
                case 'E':
                    editPlayer();
                    break;
                case 'R':
                    printRanking();
                    break;
                case 'N':
                    gTournaments[0]->enterResults();
                    break;
                case 'Q':
                    cout << endl << "Closing the program";
                    break;
                default:
                    cout << endl << "Please enter a valid command.";
                    break;
            }
        }
        else if (menuToPrint==menuEnd)
        {
            switch (command)
            {
                case 'S':
                    startNewTournament();
                    break;
                case 'V':
                    viewPlayers();
                    break;

                case 'R':
                    printRanking();
                    break;
                case 'Q':
                    cout << endl << "Closing the program";
                    break;
                default:
                    cout << endl << "Please enter a valid command.";
                    break;
            }
        }
        else
        {
            cout << endl << "A serious error has occured.";
        }

    }while (command != 'Q');

     writeToFileTournament();
     writeToFile();

    return 0;
}
