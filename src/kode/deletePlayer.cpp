#include <iostream>                //  cout, cin
#include <string>                    //  STRING-KLASSEN

#include "player.h"
#include "LesData3.h"
#include "functions.h"

using namespace std;

extern vector<Player*>playerList;

void deletePlayer()
{
    if (playerList.size()==0)
    {
        cout << endl << "No player's exist.";
        return;
    }
    int choice = 0;
    int id;
    int index = -1;
    string search;
    Player* playerToDelete= nullptr;
    do {
        cout << endl << "Choose what you wish to search by";
        choice = lesInt("\n1: search by name \nor \n2: search by playerID\n", 1, 2);
        switch (choice)
        {
            case 1:
                do
                {
                    cout << "\nPlease type either the player's first or last name\n";
                    getline(cin, search);
                    index = searchByName(search);
                    if (index == -1)
                    {
                        cout << endl << "Name not found, please try again.";
                    }
                } while(index == -1);
                playerToDelete = playerList[index];
                break;
            case 2:
                do
                {
                    id = lesInt("\nPlease type the ID of the player you wish to delete\n",0,10000);
                    index = searchByID(id,playerList);
                    if (index == -1)
                    {
                        cout << endl << "No player with this ID exists, please try again!" << endl;
                    }
                }while(searchByID(id,playerList)==-1);
                playerToDelete = playerList[index];
                break;
            default:
                cout << endl << "Invalid input, please try again";
                break;
        }
    }while(playerToDelete == nullptr);

    bool toDelete = true;
    do {
        choice = 0;
        cout << endl << "This is the player the program has found: ";
        playerToDelete->printPlayer();
        choice = lesInt("Do you wish to delete this player?\n1: yes\n2: no\n",1,2);
        switch (choice)
        {
            case 1:
                playerList[index]=playerList[playerList.size()-1];
                playerList.pop_back();
                toDelete=false;
                break;
            case 2:
                toDelete=false;
                break;
            default:
                cout << endl << "Invalid input!";
                break;
        }
    } while (toDelete);
    choice = lesInt("\nDo you wish to delete another player?\n1: yes\n2: no",1,2);
    switch (choice)
    {
        case 1:
            deletePlayer();
            break;
        case 2:
            return;
            break;
        default:
            cout << endl << "Invalid input, returning to menu.";
            return;
            break;
    }
}