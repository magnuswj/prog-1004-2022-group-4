#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"
#include "Tournament.h"
#include "functions.h"

using namespace std;

extern vector<Player*> playerList;
/**
 * edit info about a player
 */
void editPlayer()
{
    if (playerList.size()==0)
    {
        cout << endl << "No player's exist.";
        return;
    }
    int choice = 0;
    int id;
    int index = -1;
    string search;
    Player* playerToEdit = nullptr;
    do {
        cout << endl << "Choose what you wish to search by";
        choice = lesInt("\n1: search by name \nor \n2: search by playerID\n", 1, 2);
        switch (choice)
        {
            case 1:
                do
                {
                    cout << "\nPlease type either the player's first or last name\n";
                    getline(cin, search);
                    index = searchByName(search);
                    if (index == -1)
                    {
                        cout << endl << "Name not found, please try again.";
                    }
                } while(index == -1);
                playerToEdit = playerList[index];
                break;
            case 2:
                do
                {
                    id = lesInt("\nPlease type the ID of the player you wish to edit\n",0,10000);
                    index = searchByID(id,playerList);
                    if (index == -1)
                    {
                        cout << endl << "No player with this ID exists, please try again!" << endl;
                    }
                }while(searchByID(id,playerList)==-1);
                playerToEdit = playerList[index];
                break;
            default:
                cout << endl << "Invalid input, please try again";
                break;
        }
    }while(playerToEdit == nullptr);

    cout << endl << "Player found!";
    playerToEdit->printPlayer();
    cout << endl;
    choice=0;
    bool edit = true;
    int input;
    do
    {
        input = 0;
        cout << endl << "What do you wish to edit?";
        choice = lesInt("\n1: First name\n2:Last name\n3: Club\n4: Rating\n5: ID\n",1,5);
        switch (choice)
        {
            case 1:
                cout << endl << "What should the first name be?" << endl;
                getline(cin,playerToEdit->firstName);
                break;
            case 2:
                cout << endl << "What should the last name be?" << endl;
                getline(cin,playerToEdit->lastName);
                break;
            case 3:
                cout << endl << "What club does the player belong to?" << endl;
                getline(cin,playerToEdit->club);
                break;
            case 4:
                input = lesInt("\nWhat is the player's new rating?\n",0,3000);
                playerToEdit->rating = input;
                break;
            case 5:
                input = lesInt("\nWhat is the player's ID?\n",0,10000);
                playerToEdit->playerID = input;
                break;
            default:
                cout << endl << "Error: not a valid command!";
                break;
        }
        choice = 0;
        cout << endl << "Do you wish to continue editing this player?";
        choice = lesInt("\n1: yes\n2: no\n",1,2);
        if (choice == 2)
        {
            choice = 0;
            choice = lesInt("\nDo you wish to edit another player?\n1: yes\n2: no\n",1,2);
            if (choice==1)
            {
                editPlayer();
                edit=false;
            }
            else if(choice==2)
            {
                edit=false;
            }
        }
    }while(edit);
}