/**
 * Declaration of all functions
 */

#ifndef CHECKMATE_FUNCTIONS_H
#define CHECKMATE_FUNCTIONS_H

using namespace std;

void newPlayer(int id); //+
int searchByID(int id, vector<Player*> list); //+
int searchByName(string name); //+
void sortRanking(); //+
void printRanking(); //+
void viewPlayers(); //+
void editPlayer(); //+
void startNewTournament();//+
void deletePlayer();//-
void readFromFile();
void readFromFileTournament();
void writeToFile();
void writeToFileTournament();

#endif //CHECKMATE_FUNCTIONS_H
