/**
 * Reads new player
 */

#include <iostream>                //  cout, cin
#include <string>                    //  STRING-KLASSEN

#include "player.h"
#include "LesData3.h"
#include "functions.h"

using namespace std;

extern vector<Player*>playerList;

void newPlayer(int id)
{
    Player* newPlayer;
    if (searchByID(id,playerList)!=-1)
    {
        do
        {
            cout << endl << "A player with this ID already exists, please try again!" << endl;
            id = lesInt("Please offer a valid ID\n",0,10000);
        }while(searchByID(id,playerList)!=-1);
    }
    newPlayer = new Player;
    newPlayer->playerID = id;
    cout << endl << "Last name of player " << id << endl;
    getline(cin, newPlayer->lastName);
    cout << endl << "First name of player " << id << endl;
    getline(cin, newPlayer->firstName);
    newPlayer->rating = lesInt("\nRating: ", 1, 3000);
    cout << endl << "Club: ";
    newPlayer->white=0;
    getline(cin, newPlayer->club);

    playerList.push_back(newPlayer);
}