//
// Created by sigru on 25.04.2022.
//

#include <string>   //  string
#include <vector>   //  vector
#include <fstream>  //  ifstream, ofstream
#include <iostream> //  cout, cin
#include <iomanip>  //  setw

#include "player.h"
#include "LesData3.h"
#include "functions.h"

using namespace std;

extern vector<Player*>playerList;
extern vector<Player*>sortedRankings;

Player::Player()
{
    firstName = "";
    lastName = "";
    club = "";
    rating = 0;
    playerID = 0;
    score = 0.0f;
}

Player::Player(string givenName, string surName, string clubName, int rating, int ID, float currentScore)
{
    setPlayer(givenName,surName,clubName,rating,ID, currentScore);
}

void Player::setPlayer(string nm, string srNm, string clbNm, int rtng, int plyrID, float scr)
{
    firstName = nm;
    lastName = srNm;
    club = clbNm;
    rating = rtng;
    playerID = plyrID;
    score = scr;
}
/**
 * Prints the player
 *
 * @see currentRank(playerID);
 */
void Player::printPlayer()
{
        cout << endl << lastName << ", " << firstName
        << endl << "Club: " << club << endl
        << "Rating: " << rating << endl
        << "ID: " << playerID << endl
        << "Current score: " << score << endl;
        if (results.size()>0)
        {
            cout << "Current rank in tournament: " << currentRank(playerID) << endl;
        }

}

void Player::printPlayerTable()
{
    cout << endl << lastName << "\t"
    << playerID << "\t"
    << club << "\t"
    << rating;
}

void Player::calculateScore()
{
    float tempScore = 0.0f;
    for (int i = 0; i < results.size(); ++i)
    {
        tempScore += results[i];
    }

    score = tempScore / 2;
}

/**
 * Write one player to file
 *
 * @param out - the file being written to
 */
void Player::writeToFilePlayer(ofstream &out)
{
    out << firstName << '\n'
        << lastName << '\n'
        << rating << '\n'
        << playerID << '\n'
        << club << '\n'
        << white << '\n';
    for(int i = 0; i < results.size(); i++)
        out << results[i];
    out << '\n';
    for(int i = 0; i < playedAgainst.size(); i++)
        out << playedAgainst[i];
}

/**
 * Read one player from file
 *
 * @param in - the file being read from
 */
void Player::readFromFilePlayer(ifstream &in)
{
    int inn;

    getline(in, firstName);
    getline(in, lastName);
    in >> rating;       in.ignore();
    in >> playerID;     in.ignore();
    getline(in, club);
    in >> white;        in.ignore();
    while(!'\n') {
        in >> inn;
        results.push_back(inn);
    }
    while(!in.eof()) {
        in >> inn;  
        playedAgainst.push_back(inn);
    }
}

int Player::currentRank(int id)
{
    int index;
    sortRanking();
    index = searchByID(id,sortedRankings);
    if (index = -1)
    {
        cout << endl << "Error: no player found!" << endl;
        return -1;
    }

    int currentScore = sortedRankings[0]->score;
    int currentPlace = 1;

        for (int i = 0; i < index; ++i)
        {
            if (currentScore > sortedRankings[i]->score)
            {
                currentScore = sortedRankings[i]->score;
                currentPlace++;
            }
        }
        return currentPlace;
}

bool Player::hasPlayedAgainst(int id)
{

    for (int i = 0; i < playedAgainst.size(); i++)
    {

        if (playedAgainst[i] == id)
        {
            return true;
        }
    }
    return false;
}
