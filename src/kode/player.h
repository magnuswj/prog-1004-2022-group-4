//
// Created by sigru on 25.04.2022.
//

#ifndef CHECKMATE_PLAYER_H
#define CHECKMATE_PLAYER_H


#include <string>   //  string
#include <vector>   //  vector
#include <fstream>  //  ifstream, ofstream

using namespace std;

class Player
{
    public:

        string firstName;
        string lastName;
        string club;

        int rating;
        int playerID;
        int white;

        float score;

        vector <int> results;  //stores the results of each match, 0 for loss, 1 for draw and 2 for win
        vector <int> playedAgainst; //has every person they have played against

        Player();
        Player(string name, string surName, string clubName, int rating, int playerID, float score);

        void setPlayer(string nm, string , string clbNm, int rtng, int plyrID, float score); //+
        virtual void printPlayer(); //+
        virtual void printPlayerTable();
        virtual void calculateScore(); //+
        virtual void writeToFilePlayer(ofstream &out);//-
        virtual void readFromFilePlayer(ifstream &in);//-
        bool hasPlayedAgainst(int id);//-
        int currentRank(int id);//+
};
#endif //CHECKMATE_PLAYER_H
