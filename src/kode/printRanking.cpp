#include <string>   //  string
#include <vector>   //  vector
#include <fstream>  //  ifstream, ofstream
#include <iostream> //  cout, cin

#include "player.h"
#include "LesData3.h"
#include "functions.h"

using namespace std;

extern vector<Player*>playerList;
extern vector<Player*> sortedRankings;

void printRanking()
{
    if (playerList.size()==0)
    {
        cout << endl << "No player's exist.";
        return;
    }
    sortRanking();
        float currentScoreTP = sortedRankings[0]->score;
        int currentPlace = 1;
        for (int i = 0; i < sortedRankings.size(); i++)
        {
            if (currentScoreTP > sortedRankings[i]->score)
            {
                currentScoreTP = sortedRankings[i]->score;
                currentPlace++;
            }
            cout << endl << currentPlace
                 << "\t" << sortedRankings[i]->score << "pts"
                 << "\t" << sortedRankings[i]->lastName
                 << ", " << sortedRankings[i]->firstName;
        }
}