
#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"
#include "Tournament.h"
#include "functions.h"

using namespace std;

extern vector<Tournament *> gTournaments;
extern vector<Player*> playerList;
extern vector<Player*> sortedRankings;

void readFromFile() {

    ifstream inFile("PlayerList.dta");      // File object to read from

    Player* newPlayer;                      // Helping pointer to new object


    if(inFile) {
        cout << "\n\n\t\t.... READING FROM FILE: PLAYERLIST.DTA ....\n\n\n";

        while(!inFile.eof()) {              // while NOT end of file do this
            newPlayer = new Player;
            newPlayer->readFromFilePlayer(inFile);
            playerList.push_back(newPlayer);
        }
        inFile.close();
    } else {
        cout << "\n\n\t\t.... CAN NOT FIND FILE: PLAYERLIST.DTA ....\n\n\n";
    }

}

void readFromFileTournament() {
    
    Tournament *tournamentNew;              // Helping pointer to new object

    ifstream infile("tournamentData.dta");  // File object to read from

    if (infile)
    {
        cout << "\n\n\t\t.... READING FROM FILE: TOURNAMENTDATA.DTA ....\n\n\n";
        while (!infile.eof())               // while NOT end of file do this
        {
            tournamentNew = new Tournament();
            tournamentNew->readFromFileTournaments(infile);
            gTournaments.push_back(tournamentNew);
        }

        infile.close();
    } else {
        cout << "\n\n\t\t.... CAN NOT FIND FILE: TOURNAMENTDATA.DTA ....\n\n\n";
    }

    infile.close();
}