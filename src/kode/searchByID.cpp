#include <iostream>

#include "player.h"
#include "functions.h"

extern vector<Player*> playerList;
extern vector<Player*> sortedRankings;

int searchByID(int id, vector<Player*> list)
{

    for (int i = 0; i < list.size(); i++)
    {
        if (list[i]->playerID == id)
        {
            return i;
        }
    }
    return -1;
}