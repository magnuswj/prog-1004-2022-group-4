#include <iostream>                //  cout, cin
#include <string>                    //  STRING-KLASSEN

#include "player.h"
#include "functions.h"

extern vector<Player*> playerList;

int searchByName(string name)
{
    vector<Player*> searchlist;
    string playerName;
    int findSize = 0;
    int finds[10];
    int answer;

    int f;
    int l;

    for (int i = 0; i < name.length(); i++)
    {
        name[i] = tolower(name[i]);
    }
    for (f = 0; f < playerList.size(); f++)
    {
        playerName = playerList.at(f)->firstName;
        for (int j = 0; j < playerList.at(f)->firstName.length(); j++)
        {
            playerName[j] = tolower(playerName[j]);
        }

        if (playerName.find(name) == 0)
        {
            finds[findSize] = f;
            findSize += 1;
        }
    }
    for (l = 0; l < playerList.size(); l++)
    {
        playerName = playerList.at(l)->lastName;
        for (int j = 0; j < playerList.at(l)->lastName.length(); j++)
        {
            playerName[j] = tolower(playerName[j]);
        }

        if (playerName.find(name) == 0)
        {
            finds[findSize] = l;
            findSize += 1;
        }
    }

    if(findSize > 1)
    {
        cout << endl << "Choose one of these players " << endl;

        for (int i = 0; i < findSize; i++)
        {
            cout << i+1 << ": " << playerList.at(finds[i])->lastName << ", "
            << playerList.at(finds[i])->firstName << endl;
        }
        cin >> answer;
        return finds[answer - 1];
    }

    else if(findSize == 1)
    {
        return finds[0];
    }
    else
    {
        return -1;
    }
}