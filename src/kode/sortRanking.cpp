#include <vector>   //  vector

#include "player.h"
#include "functions.h"

using namespace std;

extern vector<Player*>playerList;
extern vector<Player*> sortedRankings;

void sortRanking()
{
    sortedRankings.clear();
    float currentHigh;
    vector<Player*> listUnsorted = playerList;
    vector<Player*> leadingPlayers;

    vector<int> listIDs;

    do
    {
        currentHigh = 0;
        for (int i = 0; i < listUnsorted.size(); i++)
        {
            listUnsorted[i]->calculateScore();
            if (listUnsorted[i]->score > currentHigh)
            {
                leadingPlayers.clear();
                listIDs.clear();
                leadingPlayers.push_back(listUnsorted[i]);
                listIDs.push_back(listUnsorted[i]->playerID);
                currentHigh = leadingPlayers[0]->score;
            }
            else if (listUnsorted[i]->score == currentHigh)
            {
                leadingPlayers.push_back(listUnsorted[i]);
                listIDs.push_back(listUnsorted[i]->playerID);
            }
        }
        for (int i = 0; i < leadingPlayers.size(); i++)
        {
            sortedRankings.push_back(leadingPlayers[i]);

        }
        leadingPlayers.clear();
        for (int i = 0; i < listIDs.size(); i++)
        {
            listUnsorted[searchByID(listIDs[i],listUnsorted)] = listUnsorted[listUnsorted.size()-1];
            listUnsorted.pop_back();
        }
        listIDs.clear();
    } while (listUnsorted.size() > 0);
}