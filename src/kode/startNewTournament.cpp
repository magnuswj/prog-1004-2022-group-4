//
// Created by sigru on 26.04.2022.
//

#include "Tournament.h"

#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"
#include "functions.h"

using namespace std;

extern vector<Tournament *> gTournaments;
extern vector<Player*> playerList;
extern vector<Player*> sortedRankings;

void startNewTournament()
{
    char answer;
    if (gTournaments.size()!=0)
    {
        do {
            cout << endl << "Are you certain you wish to start a new tournament?"
                 << "This will delete all existing tournament data, including the player list.";
            answer = lesChar(" Y/N\n");
        }while (answer != 'Y' && answer != 'N');
        if (answer == 'N')
        {
            return;
        }
        playerList.clear();
        sortedRankings.clear();
        gTournaments.clear();
    }
    Tournament* newTournament;
    newTournament = new Tournament;

    newTournament->nrOfTables = lesInt("\nHow many tables should the tournament have?\n",1,1000);
    newTournament->currentRound = 0;
    int maxRounds = newTournament->nrOfTables*2-1;
    newTournament->nrOfRounds = lesInt("\nHow many rounds should the tournament have?\n",1,maxRounds);
    newTournament->finished=false;
    gTournaments.push_back(newTournament);
}