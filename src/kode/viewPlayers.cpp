#include <iostream> //  cout, cin
#include <vector>   //  vector
#include "player.h"
#include "Tournament.h"

using namespace std;

extern vector<Player*> playerList;

/**
 * Prints a list of all players with identification
 */
void viewPlayers()
{
    if (playerList.size()==0)
    {
        cout << endl << "No player's exist.";
        return;
    }
    cout << endl << "Last name" << "\t"
         << "Player ID" << "\t"
         << "Club" << "\t"
         << "Rating";
    for (int i = 0; i < playerList.size(); i++)
    {
        playerList[i]->printPlayerTable();
    }
}