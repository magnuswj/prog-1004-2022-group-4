
#include <iostream> //  cout, cin
#include <string>   //  string
#include <vector>   //  vector
#include <iomanip>  //  setw
#include <fstream>  //  ifstream, ofstream
#include "LesData3.h"
#include "player.h"
#include "Tournament.h"
#include "functions.h"

using namespace std;

extern vector<Tournament *> gTournaments;
extern vector<Player*> playerList;
extern vector<Player*> sortedRankings;


void writeToFile() {

    ofstream outFile("PlayerList.dta");

    if(outFile) {
        for (int i = 0; i < playerList.size(); i++) {
            playerList[i]->writeToFilePlayer(outFile);
        }
        outFile.close();
    }
    

}


void writeToFileTournament() {

    ofstream outfile("tournamentData.dta");

    if (outfile)
    {
        for (int i = 0; i < gTournaments.size(); i++)
            gTournaments[i]->writeToFileTournaments(outfile);

        outfile.close();
    }

}
